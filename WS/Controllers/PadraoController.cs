﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WS.Controllers
{
    [RoutePrefix("ws")]
    public class PadraoController : ApiController
    {
        [Route("getTexto")]
        public String getTexto() {
            return "Olá web API!";
        }
    }
}
